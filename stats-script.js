const fs = require('fs');
const moment = require('moment');
const usercache = require('./usercache.json');

let users = {}
for (let {name, uuid} of usercache) users[uuid] = name;

stats_files = fs.readdirSync("./world/stats");

stats_list = []

for (let file of stats_files) {
    let { stats } = require("./world/stats/" + file);
    let ticks = stats["minecraft:custom"]["minecraft:total_world_time"];

    let name = users[file.substr(0,36)];
    let time = moment.duration(ticks*50).humanize();

    let hours = Math.floor(ticks / 72000);
    let minutes = Math.floor((ticks%72000)/1200);

    // if (ticks > 20*60*60) {
    //     time += ' and ' + moment.duration((ticks%72000)*50).humanize();
    // }

    stats_list.push({name, ticks, time: (hours ? hours + 'h ' : '') + minutes + 'm'});
}

stats_list.sort((a,b) => a.ticks - b.ticks);

console.table(stats_list);
